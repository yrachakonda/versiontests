import * as actionTypes from "./actionTypes";

const initialState: ArticleState = {
  articles: [
    {
      id: 1,
      title: "Science Post 1",
      body:
        "Astronomers detect black holes swallowing up neutron stars for the first time. Another piece in the gravitational wave puzzle slides into place."
    },
    {
      id: 2,
      title: "Science Post 2",
      body:
        "Scientists say there's no life on Venus — but Jupiter has potential"
    }
  ]
};

const reducer = (
  state: ArticleState = initialState,
  action: ArticleAction
): ArticleState => {
  switch (action.type) {
    case actionTypes.ADD_ARTICLE:
      const newArticle: IArticle = {
        id: Math.random(), // not really unique but it's just an example
        title: action.article.title,
        body: action.article.body
      };
      return {
        ...state,
        articles: state.articles.concat(newArticle)
      };
    case actionTypes.REMOVE_ARTICLE:
      const updatedArticles: IArticle[] = state.articles.filter(
        (article) => article.id !== action.article.id
      );
      return {
        ...state,
        articles: updatedArticles
      };
  }
  return state;
};

export default reducer;